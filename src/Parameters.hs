module Parameters (FractalType (..), rangeX, rangeY, colorF) where

import Data.Complex


data FractalType = Mandelbrot
                 | BurningShip
                 | Newton 
                 | Julia 

data Parameters = Parameters { steps :: Int,
                               min_X :: Double,
                               max_X :: Double,
                               min_Y :: Double,
                               max_Y :: Double,
                               f :: ((Double -> Double), (Double -> Double), (Double -> Double)) }


parameters :: FractalType -> Parameters
parameters Mandelbrot = Parameters { steps = 300, min_X = -1.5, max_X = 1.5, min_Y = -1.5, max_Y = 1.5, f = ((/2), (/40), (/255)) }
parameters BurningShip = Parameters { steps = 350, min_X = -1.8, max_X = -1.7, min_Y = -0.09, max_Y = 0.01, f = ((/60), (/20), (/255)) }
parameters Newton = Parameters { steps = 250, min_X = -2, max_X = 2, min_Y = -2, max_Y = 2, f = ((/60), (/20), (/255)) }
parameters Julia = Parameters { steps = 300, min_X = -2, max_X = 2, min_Y = -2, max_Y = 2, f = ((/10), (/100), (/10)) }

rangeX :: FractalType -> [Double]
rangeX fractal = [min_X p, min_X p + ((max_X p - min_X p) / (fromIntegral $ steps p)) .. max_X p]
                 where p = parameters fractal
  
rangeY :: FractalType -> [Double]
rangeY fractal = [min_Y p, min_Y p + ((max_Y p - min_Y p) / (fromIntegral $ steps p)) .. max_Y p]
                 where p = parameters fractal

colorF :: FractalType -> ((Double -> Double), (Double -> Double), (Double -> Double)) 
colorF fractal =  f $ parameters fractal
