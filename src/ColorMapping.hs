module ColorMapping where

import Data.Colour.SRGB.Linear (rgb)
import Data.Colour.RGBSpace


-- | The 'rgb2' function is a replacement for standard rgb, but accepts a list of numbers instead of three arguments.
rgb2 :: [Double] -> Colour Double
rgb2 (r:g:b:[]) = rgb r g b
rgb2 _ = rgb 0 0 0

-- | The 'normalize' function transforms a number into its normalized form - i.e. between 0 and 1.
-- If the number is too small or too large, closest value is used (0 or 1)
normalize :: (Double -> Double) -> Double -> Double
normalize f x
 | f x > 1 = 1
 | f x < 0 = 0
 | otherwise = f x

-- | The 'mapColor' function transforms a number (our solution) to a specific color.
-- It accepts three functions as arguments, which are then applied to red, green and blue factors correspondingly. 
mapColor :: ((Double -> Double), (Double -> Double), (Double -> Double)) -> Double -> Colour Double
mapColor (fr, fg, fb) x = rgb2 $ map (\f -> normalize f x) [fr, fg, fb]
