module Julia (julia) where

import Data.Complex
import ColorMapping


-- | Calculates complex sequence elements according to Julia Set formula.
julia :: Complex Double -> Complex Double -> Double -> Double -> Double
julia c z i maxIter
 | i > maxIter = 0
 | otherwise = let z' = z^2 + c in
               if magnitude z' > 2
               then i
               else julia c z' (i + 1) maxIter
