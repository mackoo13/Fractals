module Ship (ship) where

import Data.Complex
import ColorMapping


-- | Calculates complex sequence elements according to Michael Michelitsch formula.
ship :: Complex Double -> Complex Double -> Double -> Double -> Double
ship c z i maxIter
    | i > maxIter = 0
    | otherwise = let z' = (abs(realPart z) :+ abs(imagPart z))^2 + c in
                  if magnitude z' > 2
                  then i
                  else ship c z' (i + 1) maxIter
