module Newton (newton) where

import Data.Complex
import ColorMapping


-- How close successive tests must be before we declare they have converged
minDiff = 0.0001

-- formula to iterate
f z = 3 * z^4 - 1
-- derivative
f' z = 12 * z^3

-- | Calculates complex sequence elements according to Newton's formula.
newton :: Complex Double -> Double -> Double -> Double 
newton z i maxIter
    | i > maxIter = 0
    | (abs $ magnitude r - magnitude z) > minDiff = newton r (i + 1) maxIter
    | otherwise = i
    where r = z - f z / f' z
