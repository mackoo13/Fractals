# Fractals

![Example](screenshots/newton.PNG "Example")

To build the program:

```
stack setup
```

```
stack build
```

And then to run it:

```
stack exec fractals-exe -- -o output_file.svg -w 600
```

Run tests:

```
stack test
```

