module Main where
import Diagrams.Prelude hiding ((<>))
import Diagrams.Backend.SVG.CmdLine
import Diagrams.Backend.CmdLine
import Data.Complex
import Data.List.Split

import Parameters
import Julia
import Newton
import Ship
import ColorMapping


drawHorizontal:: FractalType -> Double -> Complex Double -> Diagram B
drawHorizontal Mandelbrot g c = hcat (map (\r -> square 1 # lw none # fc (mapColor (colorF Mandelbrot) $ julia (r :+ g) c 0 42)) (rangeX Mandelbrot))
drawHorizontal BurningShip g c = hcat (map (\r -> square 1 # lw none # fc (mapColor (colorF BurningShip) $ ship (r :+ g) c 0 42)) (rangeX BurningShip))
drawHorizontal Newton g _ = hcat (map (\r -> square 1 # lw none # fc (mapColor (colorF Newton) $ newton (r :+ g) 0 42)) (rangeX Newton))
drawHorizontal Julia g c = hcat (map (\r -> square 1 # lw none # fc (mapColor (colorF Julia) $ julia c (r :+ g) 0 42)) (rangeX Julia))

drawVertical:: FractalType -> [Double] -> Complex Double -> Diagram B
drawVertical fractalType r c = vcat (map (\g -> drawHorizontal fractalType g c) r)


mandelbrotDiagram:: Diagram B
mandelbrotDiagram = drawVertical Mandelbrot (rangeY Mandelbrot) (0 :+ 0) 

burningShipDiagram:: Diagram B
burningShipDiagram = drawVertical BurningShip (rangeY BurningShip)  (0 :+ 0)

newtonDiagram:: Diagram B
newtonDiagram = drawVertical Newton (rangeY Newton)  (0 :+ 0) 

juliaDiagram:: Complex Double -> Diagram B
juliaDiagram = drawVertical Julia (rangeY Julia)  

makeComplex:: [String] -> Complex Double
makeComplex (c_real:c_im:[]) = ((read c_real::Double) :+ (read c_im ::Double))
makeComplex _ = error "Wrong number format."

welcomeMessage:: String
welcomeMessage = "\nChoose diagram type:\n" ++
                 " [n] for Newton fractal\n" ++ 
                 " [m] for Mandelbrot set\n" ++ 
                 " [s] for burning ship fractal\n" ++ 
                 " [j] for Julia set"

juliaSetMessage:: String
juliaSetMessage = "Enter c parameter.\n" ++ 
                  "Format: real_part imaginary_part\n" ++
                  "Recommended values:\n" ++
                  "0.285 0.01\n" ++ 
                  "-0.70176 -0.3842\n" ++ 
                  "-0.8 0.156\n"
                  
errorMessage:: String
errorMessage = "We don't support this fractal. Sorry."


main = do
    putStrLn welcomeMessage
    diagram <- getLine
    case diagram of 
            ("m") -> mainWith mandelbrotDiagram
            ("s") -> mainWith burningShipDiagram 
            ("n") -> mainWith newtonDiagram 
            ("j") -> do
                        putStrLn juliaSetMessage
                        c <- getLine
                        mainWith $ juliaDiagram $ makeComplex $ splitOn " " c
            otherwise ->
              do 
                putStrLn errorMessage
                main
                  
