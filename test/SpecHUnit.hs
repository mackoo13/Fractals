{-# LANGUAGE TemplateHaskell #-}

import Test.QuickCheck.All
import Test.QuickCheck
import Test.Framework (testGroup, Test, defaultMain)
import Test.Framework.Providers.HUnit
import Test.HUnit hiding (Test)

import Data.Colour.SRGB.Linear (rgb)
import Data.Colour.RGBSpace
import Data.Complex
import ColorMapping
import Julia


f1 :: (Double -> Double)
f1 = (* 2)

f2 :: (Double -> Double)
f2 = (* 5)

f3 :: (Double -> Double)
f3 = (/ 10)


prop_normalize_1 :: Double -> Bool
prop_normalize_1 x = ((normalize f1 x <=1) && (normalize f1 x >= 0))

prop_normalize_2 :: Double -> Bool
prop_normalize_2 x = ((normalize f2 x <=1) && (normalize f2 x >= 0))

prop_rgb_2 :: [Double] -> Bool
prop_rgb_2 xs@(r:g:b:[]) = rgb2 xs == rgb r g b
prop_rgb_2 xs = rgb2 xs == rgb 0 0 0

prop_mapColor :: Double -> Bool
prop_mapColor x = if all (\x -> x >= 0 && x <=1) $ map (\f -> f x) [f1, f2, f3] 
        then colour == rgb (f1 x) (f2 x) (f3 x) 
        else True
        where colour = mapColor (f1, f2, f3) x 


 
rgb2Suite :: Test
rgb2Suite = testGroup "test rgb2"
   [testCase "testRgb2" $ assertEqual "testRgb2" ((rgb2 [0.1, 0.2, 0.5]) == (rgb 0.1 0.2 0.5)) True,
    testCase "testRgb2TooShortList" $ assertEqual "testRgb2TooShortList" ((rgb2 [0.1, 0.1]) == (rgb 0 0 0)) True,
    testCase "testRgb2TooLongList" $ assertEqual "testRgb2TooLongList" ((rgb2 [0.5, 0.6, 0.1, 0.1]) == (rgb 0 0 0)) True]

normalizeSuite :: Test
normalizeSuite = testGroup "test normalize" 
    [testCase "testNormalize" $ assertEqual "testNormalize" (normalize (/60) 12) 0.2,
    testCase "testNormalizeBelow0" $ assertEqual "testNormalizeBelow0" (normalize (*2) (-0.3)) 0,
    testCase "testNormalizeAbove1" $ assertEqual "testNormalizeAbove1" (normalize (*4) 0.3) 1]
    
mapColorSuite :: Test
mapColorSuite = testGroup "test mapColor" 
    [testCase "testMapColor" $ assertEqual "testMapColor" ((mapColor ((*2), (+0.1), (/2)) 0.3) == rgb 0.6 0.4 0.15) True]

juliaSuite :: Test
juliaSuite = testGroup "test julia"
    [testCase "testJulia" $ assertEqual "test Julia" (julia ((-0.45) :+ (-0.1428)) ((-0.056) :+ 0.123) 0 42) 0,
    testCase "testJuliaMaxIter" $ assertEqual "testJuliaMaxIter" (julia ((-0.45) :+ (-0.1428)) (1 :+ 7) 65 42) 0]

return []
main :: IO ()
main = $quickCheckAll >> defaultMain [rgb2Suite, normalizeSuite, mapColorSuite, juliaSuite]
